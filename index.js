// Lesson 3
function fizzBuzz () {
    /*
    Takes integers starting from 1 till 100,
    if an integer is multiple to 3 returns "Fizz", 
    if an integer is multiple to 5 returns "Buzz",
    if an integer is multiple to 3 and 5 returns "FizzBuzz"
    */
   for (let int = 1; int<=100; int++) {
       if (int % 3 == 0 && int % 5 == 0) {
           console.log("FizzBuzz");
       }else if (int % 3 == 0) {
           console.log("Fizz");
       }else if (int % 5 == 0) {
           console.log("Buzz");
       }else console.log(int);
   }
   return "Finished";
}


function filterArray (array) {
    /*
    Takes an Array as an argument,
    sorts nested arrays in the Array
    Returns ascendingly sorted array
    */
    var uniqueSortedArr = [];
    for (item of array) {
        if (Array.isArray(item)) {
            for (i of item) {
                if (!uniqueSortedArr.includes(i)) {
                    uniqueSortedArr.push(i);
                }
            }            
        }
    }
    return uniqueSortedArr.sort(function (a,b) {return a-b});
};

// Output DON'T TOUCH!
console.log(fizzBuzz())
console.log(filterArray([ [2], 23, 'dance', true, [3, 5, 3] ]));
